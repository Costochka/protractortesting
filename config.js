exports.config = {
    framework: 'jasmine2',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./specs/trial.js'],
    capabilities: {
		browserName: 'chrome'
		, shardTestFiles: true
		, maxInstances: 5
  },
   onPrepare: function() {
    const AllureReporter = require('jasmine-allure-reporter');
    jasmine.getEnv().addReporter(new AllureReporter({
      resultsDir: 'allure-results'
    }));
  }
  
    /*
    multiCapabilities: [{
  'browserName': 'firefox'
        }, {
  'browserName': 'chrome'
    }]*/
};