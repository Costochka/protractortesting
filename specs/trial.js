describe('Protractor Test Banking page', () => {
    beforeEach(async () => {
        await browser.get('http://www.way2automation.com/angularjs-protractor/banking/#/login');
    });

    it('should have a title', async () => {
       await expect(browser.getTitle()).toEqual('Protractor practice website - Banking App');
    });

    it('should successfully pass to Customer Login', async () => {
        await $('div.center:nth-child(1) > button:nth-child(1)').click();
        await expect($('.form-group label').getText()).toEqual('Your Name :');
    });

    it('should successfully pass to Bank Manager Login', async () => {
        await $('div.center:nth-child(3) > button:nth-child(1)').click();
        const btnArr = await $$('.center button').map(el => el.getText());
        await expect(btnArr).toEqual(['Add Customer','Open Account','Customers']);
    });
});